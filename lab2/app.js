var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var morgan = require('morgan');
const bodyParser = require('body-parser');
const busboyBodyParser = require('busboy-body-parser');

var usersRouter = require('./routes/users');
var courseRouter = require('./routes/courses')


const port = 3000;
const user = require("./models/user.js")
const UserRepository = require('./repositories/userRepository.js')
const users = new UserRepository("./data/users.json")



const courseRepository = require('./repositories/courseRepository.js')
const course = require('./models/course.js')
const courses = new courseRepository("./data/courses.json")

var app = express();


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
const apiRouter = require('./routes/apiRouter');
app.use('', apiRouter);
 
const expressSwaggerGenerator = require('express-swagger-generator');
const mediaRouter = require('./routes/mediaRouter');
const expressSwagger = expressSwaggerGenerator(app);
 
const options = {
    swaggerDefinition: {
        info: {
            description: 'TODO: Change this description',
            title: 'TODO: Change this title',
            version: '1.0.0',
        },
        host: `localhost:${port}`,
        produces: [ "application/json" ],
    },
    basedir: __dirname,
    files: ['./routes/**/*.js', './models/**/*.js'],
};
expressSwagger(options);
app.set('port', (process.env.PORT || 3000));
app.listen(port, function(err){
    if (err) console.log("Error in server setup")
    console.log("Server listening on Port", port);
})
app.use(morgan('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(busboyBodyParser());

app.use('/api/users', usersRouter)
app.use('/api/courses', courseRouter)
app.use('/api/media', mediaRouter)

app.use(morgan('dev'));


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

console.log("OK")
