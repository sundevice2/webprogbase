const courseRepository = require('../repositories/courseRepository')
const CourseRepository = new courseRepository('./data/courses.json');

module.exports = {
    getCourseById(req, res) {
        try {
            const id = parseInt(req.params.id);
            if (isNaN(id) || id < 1) {
                res.status(400).send('Incorrect id');
            }
            else {
                const course = CourseRepository.getCourseId(id);
                if (course) {
                    console.log(course);
                    res.status(200).send(course);
                }
                else {
                    res.status(404).send('Not found');
                }
            }
        }
        catch (e) {
            console.log(e.message);
            res.status(500).send('Server error!');
        }
    },
    getCourses(req, res) {
        try {
            const strPage = req.query.page;
            const strPerPage = req.query.per_page;
            let page, per_page;
            if (strPage === undefined) {
                page = 1;
            }
            else {
                page = parseInt(strPage);
            }
            if (strPerPage === undefined) {
                per_page = 8;
            }
            else {
                per_page = parseInt(strPerPage);
            }
            if (isNaN(page) || isNaN(per_page) || page < 1 || per_page < 1) {
                res.status(400).send('Incorrect pages');
            }
            else {
                const courses = CourseRepository.getAllCourses()
                const size = courses.length;
                if (courses) {
                    const offset = per_page * (page - 1);
                    if (offset >= size) {
                        res.status(400).send('Page number is too big');
                    }
                    else {
                        const coursesPage = courses.slice(offset, offset + per_page)
                        res.status(200).send(coursesPage);
                    }
                }
                else {
                    res.status(404).send('Not found');
                }
            }
        }
        catch (e) {
            console.log(e.message);            
            res.status(500).send('Server error!');
        }
    },
    addCourse(req, res) {
        try {
            const id = CourseRepository.addCourse(req.body);
            const course = CourseRepository.getCourseId(id)
            res.status(201).send(course);
        }
        catch (e) {
            console.log(e.message);
            res.status(500).send('Server error!');
        }
    },
    updateCourse(req, res) {
        try {
            const course = CourseRepository.UpdateCourse(req.body);
            if (course) {
                
                res.status(200).send(course);
            }
            else {
                res.status(404).send('Course not found');
            }
        }
        catch (e) {
            console.log(e.message);
            res.status(500).send('Server error!');
        }
    },
    deleteCourse(req, res) {
        try {
            const id = parseInt(req.params.id);
            if (isNaN(id) || id < 1) {
                res.status(400).send('Inorrect id');
            }
            else {
                const course = CourseRepository.deleteCourse(id);
                if (course) {
                    res.status(200).send(req.course);
                    res.status(200).send(course);
                }
                else {
                    res.status(404).send('Course not found');
                }
            }
        }
        catch (e) {
            console.log(e.message)
            res.status(500).send('Server error!');
        }
    }
};

