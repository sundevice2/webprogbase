const UserRepository = require('./../repositories/userRepository');
const userRepository = new UserRepository('./data/users.json');

module.exports = {
    getUserById(req, res) {
        try {
            const id = parseInt(req.params.id);
            if (isNaN(id) || id < 1) {
                res.status(400).send('Incorrect id');
            }
            else {
                const user = userRepository.getUserById(id);
                if (user) {
                    res.status(200).send(user);
                }
                else {
                    res.status(404).send('Not found');
                }
            }
        }
        catch (e) {
            console.log(e);
            res.status(500).send('Server error!');
        }
    },
    getUsers(req, res) {
        try {
            const strPage = req.query.page;
            const strPerPage = req.query.per_page;
            let page, per_page;
            if (strPage === undefined) page = 1;
            else page = parseInt(strPage);

            if (strPerPage === undefined) per_page = 8;
            else per_page = parseInt(strPerPage);
            
            if (isNaN(page) || isNaN(per_page) || page < 1 || per_page < 1) {
                res.status(400).send('Incorrect pages');
            }
            else {
                const users = userRepository.getAllUsers();
                if (users) {
                    const size = users.length;
                    const offset = per_page * (page - 1);
                    if (offset >= size) {
                        res.status(400).send('Page number is too big');
                    }
                    else {
                        usersPage = users.slice(offset, offset + per_page)
                        res.status(200).send(usersPage);
                    }
                }
                else {
                    res.status(404).send('Not found');
                }
            }
        }
        catch (e) {
            res.status(500).send('Server error!');
        }
    }
};
