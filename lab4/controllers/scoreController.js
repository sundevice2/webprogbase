const ScoreRepository = require('../repositories/scoreRepository')
const scoreRepository = new ScoreRepository();

const UserRepository = require('../repositories/userRepository')
const userRepository = new UserRepository();

const ScoreRepository = require('../repositories/scoreRepository')
const scoreRepository = new ScoreRepository();

module.exports = {
    async getScoreById(req, res) {
        try {
            const id = req.params.id;
            const score = await scoreRepository.getScoreById(id);
            if (score) {
                res.status(200).render('score', {
                    headTitle: 'Scores', scoresCurrent:
                        'current', score: score
                });
            }
            else {
                res.status(404).render('score', {
                    headTitle: 'Scores', scoresCurrent: 'current',
                    score: null, msg: '404! No such score.'
                });
            }
        }
        catch (e) {
            console.log(e);
            res.status(500).render('score', {
                headTitle: 'Scores', scoresCurrent: 'current',
                score: null, msg: 'Server error!'
            });
        }
    },

    async getScores(req, res) {
        try {
            const strPage = req.query.page;
            const per_page = 4;
            let page;
            if (strPage === undefined) page = 1;
            else page = parseInt(strPage);
            if (page < 1 || isNaN(page)) page = 1;

            let scores = await scoreRepository.getScores();

            if (scores) {
                const size = scores.length;
                const offset = per_page * (page - 1);
                let maxPage = Math.ceil(size / per_page);
                const scoresPage = scores.slice(offset, offset + per_page);
                const leftPage = [];
                const rightPage = [];

                if (page === 1) leftPage.push({ curPage: page });
                else leftPage.push({ curPage: page, page: page - 1 });

                if (maxPage === 0 || size === 0) {
                    maxPage = 1;
                    page = 1;
                }
                else if (page > maxPage) page = maxPage;

                if (offset + per_page < size)
                    rightPage.push({ curPage: page, page: page + 1 });
                else rightPage.push({ curPage: page });

                const params = {
                    headTitle: 'Scores', scoresCurrent: 'current',
                    scoresPage: scoresPage, leftPage: leftPage,
                    rightPage: rightPage, maxPage: maxPage
                };
                res.status(200).render('scores', params);
            }
            else {
                res.status(200).render('scores', {
                    headTitle: 'Scores', scoresPage: null,
                    scoresCurrent: 'current'
                });
            }
        }
        catch (e) {
            console.log(e);
            res.status(500).render('scores', {
                headTitle: 'Scores', scoresCurrent: 'current',
                score: null, msg: 'Server error!'
            });
        }
    },

    async addScore(req, res) {
        try {
            if (!req.body.scoreDate) {
                req.body.scoreDate = new Date();
            }
            const id = await scoreRepository.addScore(req.body);
            res.status(303).redirect('/scores/' + id);
        }
        catch (e) {
            console.log(e);
            res.status(500).render('score', {
                headTitle: 'Scores', scoresCurrent: 'current',
                score: null, msg: 'Server error!'
            });
        }
    },

    async updateScore(req, res) {
        try {
            const id = req.params.id;
            const updatedInfo = req.body;
            const updatedScore = await scoreRepository.updateScore(id, updatedInfo);
            if (updatedScore) {
                res.status(303).redirect('/scores/' + id);
            }
            else {
                res.status(404).render('updateScore', {
                    headTitle: 'Scores', scoresCurrent: 'current',
                    score: null, msg: '404! No such score.'
                });
            }
        }
        catch (e) {
            console.log(e);
            res.status(500).render('updateScore', {
                headTitle: 'Scores', scoresCurrent: 'current',
                score: null, msg: 'Server error!'
            });
        }
    },

    async deleteScoreById(req, res) {
        try {
            const id = req.params.id;
            const score = scoreRepository.deleteScore(id);
            if (score) {
                res.status(303).redirect('/scores');
            }
            else {
                res.status(404).render('score', {
                    headTitle: 'Scores', scoresCurrent: 'current',
                    score: null, msg: '404! No such score.'
                });
            }
        }
        catch (e) {
            console.log(e);
            res.status(500).render('score', {
                headTitle: 'Scores', scoresCurrent: 'current',
                score: null, msg: 'Server error!'
            });
        }
    },

    async getAndUpdateScore(req, res) {
        try {
            const id = req.params.id;
            const score = await scoreRepository.getScoreById(id);
            if (score) {
                res.status(200).render('updateScore', {
                    headTitle: 'Update the score', scoresCurrent: 'current',
                    score: score
                });
            }
            else {
                res.status(404).render('score', {
                    headTitle: 'Scores', scoresCurrent: 'current',
                    score: null, msg: '404! No such score.'
                });
            }
        }
        catch (e) {
            console.log(e);
            res.status(500).render('score', {
                headTitle: 'Scores', scoresCurrent: 'current',
                score: null, msg: 'Server error!'
            });
        }
    },

    async newScore(req, res) {
        const users = await userRepository.getUsers();
        const scores = await scoreRepository.getScores();
        let scoreList = new Array();
        let userList = new Array();
        users.forEach(x => userList.push(x._id, x.login));
        scores.forEach(w => scoreList.push(w._id, w.name));
        res.status(200).render('newScore', {
            headTitle: 'Create a new score', scoresCurrent: 'current',
            scoreList: scoreList, userList: userList
        });
    }
};
