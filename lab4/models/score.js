const mongoose = require('momgoose')

const ScoresSchema = new mongoose.Schema({
    userId: { type: mongoose.mongo.ObjectId, ref: "users"},
    courseId: { type: mongoose.mongo.ObjectId, ref: "courses" },
    score:  { type: Number, required: true },
    ScoreDate: { type: Date },
    img: String
});

const Course = mongoose.model('courses',CoursesSchema);

module.exports = Course;