const Score = require('../models/score.js');

class ScoreRepository {

    async getScores() {
        const scores = await Score.find().populate('courseId').populate('userId');
        return scores;
    }

    async getScoreById(id) {
        const score = await Score.findById(id).populate('courseId').populate('userId');
        return score;
    }

    async addScore(scoreModel) {
        const score = new Score(scoreModel);
        const newScore = await score.save();
        return newScore.id;
    }

    async updateScore(id, updatedInfo) {
        const updatedScore = await Score.findByIdAndUpdate(id, { $set: updatedInfo });
        return updatedScore;
    }

    async deleteScore(id) {
        const deletedScore = await Score.deleteOne({ _id: id });
        return deletedScore;
    }
}

module.exports = ScoreRepository;