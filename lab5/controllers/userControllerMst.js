const path = require('path');
const UserRepository = require('../repositories/userRepository');
const userRepository = new UserRepository(path.resolve(__dirname, '../data/users.json'));

module.exports = {
    async getUserById(req, res) {
        try {
            const id = req.params.id;
            const user = await userRepository.getUserById(id);
            if (user) {
                if (user.biography == null) user.biography = 'No bio';
                res.status(200).render('user', {
                    headTitle: 'User', usersCurrent: 'current',
                    user: user
                });
            }
            else {
                res.status(404).render('users', {
                    headTitle: 'Users', usersCurrent: 'current',
                    user: null, msg: '404! No such user.'
                });
            }
        }
        catch (e) {
            console.log(e);
            res.status(500).render('user', {
                headTitle: 'User', usersCurrent: 'current',
                user: null, msg: 'Server error!'
            });
        }
    },

    async getUsers(req, res) {
        try {
            const strPage = req.query.page;
            const per_page = 4;
            let page;
            if (strPage === undefined) page = 1;
            else page = parseInt(strPage);
            if (page < 1 || isNaN(page)) page = 1;

            const users = await userRepository.getUsers();
            if (users) {
                const size = users.length;
                const offset = per_page * (page - 1);
                let maxPage = Math.ceil(size / per_page);
                usersPage = users.slice(offset, offset + per_page);
                const leftPage = [];
                const rightPage = [];
                if (page === 1) leftPage.push({ curPage: page });
                else leftPage.push({ curPage: page, page: page - 1 });

                if (maxPage === 0 || size === 0) {
                    maxPage = 1;
                    page = 1;
                }
                else if (page > maxPage) page = maxPage;

                if (offset + per_page < size)
                    rightPage.push({ curPage: page, page: page + 1 });
                else rightPage.push({ curPage: page });

                const params = {
                    headTitle: 'Users', usersCurrent: 'current',
                    usersPage: usersPage, leftPage: leftPage,
                    rightPage: rightPage, maxPage: maxPage, users: users
                };
                res.status(200).render('users', params);
            }
            else {
                res.status(200).render('users', {
                    headTitle: 'Users', usersPage: null,
                    usersCurrent: 'current', users: users
                });
            }
        }
        catch (e) {
            console.log(e);
            res.status(500).render('users', {
                headTitle: 'Users', usersCurrent: 'current',
                user: null, msg: 'Server error!'
            });
        }
    }
};