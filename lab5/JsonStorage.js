// jsonStorage.js
const fs = require('fs');


class JsonStorage {

    constructor(filePath) {
        this.filePath = filePath;
    }

    get nextId() {
        const nextId = this.readAll().nextId;
        return nextId;

    }

    incrementNextId() {
        try {
            const jsonAll = this.readAll();
            jsonAll.nextId = jsonAll.nextId + 1;
            this.writeId(jsonAll.nextId);
            return jsonAll.nextId;
        }
        catch {
            throw new Error("Not implemented.");
        }
    }

    readAll() {
        const jsonText = fs.readFileSync(this.filePath);
        const jsonAll = JSON.parse(jsonText);
        return jsonAll;
    }

    readItems() {
        const jsonText = fs.readFileSync(this.filePath);
        if (jsonText.length === '0')
            this.parsedItems = { "nextId": 1, "items": [] };
        else {
            const items = jsonText.toString();
            this.parsedItems = JSON.parse(items);
        }
        return this.parsedItems.items;
    }

    writeId(id) {
        const file = this.readAll();
        file.nextId = id;
        fs.writeFileSync(this.filePath, JSON.stringify(file, null, 4));
    }

    writeItems(items) {
        const file = this.readAll();
        file.items = items;
        fs.writeFileSync(this.filePath, JSON.stringify(file, null, 4));
    }
};

module.exports = JsonStorage;