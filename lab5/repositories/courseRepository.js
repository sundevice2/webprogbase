const Course = require("../models/course.js")
const JsonStorage = require("../JsonStorage.js")

class courseRepository {
    constructor(filePath) {
        this.storage = new JsonStorage(filePath)
    }
    getCourses() {
        const courses = this.storage.readItems();
        return courses;
    }
    getCoursesById(id) {
        const items = this.storage.readItems();
        let course = null;
        for (const item of items) {
            if (item.id == id) {
                course = new Course(item.id, item.name, item.duration, item.group, item.credits, item.date, item.img)
            }
        }
        return course;
    }
    getCoursesByName(name) {
        let items = this.storage.readItems();
        items = items.filter(item => item.name.includes(name));
        return items;
    }
    addCourse(entityModel) {
        const courses = this.storage.readItems();
        entityModel.id = this.storage.nextId;
        this.storage.incrementNextId();
        courses.unshift(entityModel);
        this.storage.writeItems(courses);
        return entityModel;
    }
    deleteCourse(entityId) {
        const items = this.storage.readItems();
        for (let i = 0; i < items.length; i++) {
            if (items[i].id == entityId) {
                const course = items[i];
                items.splice(i, 1);
                this.storage.writeItems(items);
                return course;
            }
        }
        return null;
    }


}

module.exports = courseRepository;