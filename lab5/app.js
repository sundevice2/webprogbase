const express = require('express');
const mustache_ex = require('mustache-express');
const path = require('path');
require('dotenv').config();
const app = express();
const connectOptions = {
    useNewUrlParser: true,
    useUnifiedTopology: true,
};
const ws = require('ws');
const http = require('http');
const server = http.createServer(app);
const wsServer = new ws.Server({ server });

const morgan = require('morgan');
const body_parser = require('body-parser');
const busboy_body_parser = require('busboy-body-parser');
const apiRouter = require('./routes/apiRouter');
const mstRouter = require('./routes/mstRouter');
const { mustache } = require('consolidate');

app.use(body_parser.urlencoded({ extended: true }));
app.use(body_parser.json());
app.use(busboy_body_parser());

app.use((err, req, res, next) => {
    if (err instanceof SyntaxError && err.status === 400 && 'body' in err) {
        res.status(400).send({ mess: "Bad request" });
        return;
    }

    next();
});


const viewsDir = path.join(__dirname, 'views');
app.engine("mst", mustache_ex(path.join(viewsDir, "partials")));
app.set('views', viewsDir);
app.set('view engine', 'mst');

app.use(express.static(path.join(__dirname, './public')));

const connections = require("./websocket/websocketArr").webConnArr;

wsServer.on("connection", connection => {
    connections.push(connection);
    console.log("new connection. total connections:", connections.length);

    connection.on("close", () => {
        connections.splice(connections.indexOf(connection), 1);
        console.log("connection lost. total connections:", connections.length);
    });
});

const fileOptions = { root: path.join(__dirname, './views'), };
app.get('/about', (req, res) => {
    res.render('about', {
        headTitle: 'About', aboutCurrent: 'current'
    });
});
app.get('/', (req, res) => {
    res.render('index', {
        headTitle: 'Home', homeCurrent: 'current'
    });
});

app.use('/api', apiRouter);
app.use('', mstRouter);
app.use(express.static("./public"));
app.use(express.static("./data"));
app.use(morgan('dev'));

const port = 3000;
server.listen(port, () => console.log(`Web server started at ${port}`));