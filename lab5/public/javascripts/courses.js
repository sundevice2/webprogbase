let template_table = "";
let pages = [];
window.addEventListener('load', async () => {
    try {
        template_table = await (await fetch("/templates/table.mst")).text();
        let course_data = await (await fetch("/api/courses")).json();
        let i = 1;
        while (i <= course_data.maxPage) {
            pages.push({ page: i, name: course_data.nameSearch });
            i++;
        }
        course_data.pages = pages;

        const renderedHtmlStr = Mustache.render(template_table, course_data);
        const appEl = document.getElementById('courseTable');
        appEl.innerHTML = renderedHtmlStr;
        document.getElementById("pageLink1").className += " active";
        AddEvents();
    }
    catch (err) {
        console.log(err);
    }
});

async function choosePage(e) {
    e.preventDefault();
    document.getElementById("tableSpinner").style.display = "";
    document.getElementById("courseTable").style.display = "none";
    const itemsData = await (await fetch("/api" + e.target.attributes.href.value)).json();
    itemsData.pages = pages;
    const renderedHtmlStr = Mustache.render(template_table, itemsData);
    RemoveEvents();
    const appEl = document.getElementById('courseTable');
    appEl.innerHTML = renderedHtmlStr;
    document.getElementById("pageLink" + itemsData.page).className += " active";
    AddEvents();

    document.getElementById("tableSpinner").style.display = "none";
    document.getElementById("courseTable").style.display = "";
}

async function changePage(e) {
    try {
        e.preventDefault();
        document.getElementById("tableSpinner").style.display = "";
        document.getElementById("courseTable").style.display = "none";

        const itemsData = await (await fetch("/api" + e.target.attributes.href.value)).json();
        itemsData.pages = pages;
        const renderedHtmlStr = Mustache.render(template_table, itemsData);
        RemoveEvents();
        const appEl = document.getElementById('courseTable');
        appEl.innerHTML = renderedHtmlStr;
        document.getElementById("pageLink" + itemsData.page).className += " active";
        AddEvents();
        document.getElementById("tableSpinner").style.display = "none";
        document.getElementById("courseTable").style.display = "";
    }
    catch (err) {
        console.log(err);
    }

}

async function findCoursesByName(e) {
    try {
        e.preventDefault();
        document.getElementById("tableSpinner").style.display = "";
        document.getElementById("courseTable").style.display = "none";
        const itemsData = await (await fetch("/api/courses?name=" + e.target.value + "&page=")).json();
        let i = 1;
        pages = [];
        while (i <= itemsData.maxPage) {
            pages.push({ page: i, name: itemsData.nameSearch });
            i++;
        }
        itemsData.pages = pages;
        const renderedHtmlStr = Mustache.render(template_table, itemsData);

        RemoveEvents();
        const appEl = document.getElementById('courseTable');
        appEl.innerHTML = renderedHtmlStr;
        document.getElementById("pageLink" + itemsData.page).className += " active";
        AddEvents();
        document.getElementById("tableSpinner").style.display = "none";
        document.getElementById("courseTable").style.display = "";
    }
    catch (err) {
        console.log(err);
    }
}

function AddEvents() {
    document.getElementById("findCourse").addEventListener("input", findCoursesByName);
    document.getElementById("nextCoursePage").addEventListener("click", changePage);
    document.getElementById("previousCoursePage").addEventListener("click", changePage);
    const htmlPages = document.getElementsByClassName("pageLink");
    for (let page of htmlPages) {
        page.addEventListener("click", choosePage);
    }
}

function RemoveEvents() {
    document.getElementById("findCourse").removeEventListener("input", findCoursesByName);
    document.getElementById("nextCoursePage").removeEventListener("click", changePage);
    document.getElementById("previousCoursePage").removeEventListener("click", changePage);
    const htmlPages = document.getElementsByClassName("pageLink");
    for (let page of htmlPages) {
        page.removeEventListener("click", choosePage);
    }
}