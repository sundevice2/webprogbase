let templateStr = "";
window.addEventListener('load', async () => {
    try {
        templateStr = await (await fetch("/templates/newCourse.mst")).text();
        const courses = await (await fetch("/api/courses?per_page=10")).json();
        const renderedHtmlStr = Mustache.render(templateStr, courses);
        const appEl = document.getElementById("app");
        appEl.innerHTML = renderedHtmlStr;
        document.getElementById("createCourse").addEventListener("submit", createCourse);
    }
    catch (err) {
        console.log(err)
    }

});

async function createCourse(e) {
    try {
        e.preventDefault();
        document.getElementById("spinner").style.display = ""
        document.getElementById("app").style.display = "none"
        const course_data = new FormData(this);
        await (fetch("/api/courses/", { method: "POST", body: course_data }));
        const courses = await (await fetch("/api/courses?per_page=10")).json();
        const renderedHtmlStr = Mustache.render(templateStr, courses);
        document.getElementById("createCourse").removeEventListener("submit", createCourse);
        const appEl = document.getElementById("app");
        appEl.innerHTML = renderedHtmlStr;
        document.getElementById("createCourse").addEventListener("submit", createCourse);
        document.getElementById("spinner").style.display = "none"
        document.getElementById("app").style.display = ""
    }
    catch (err) {
        console.log(err)
    }
}