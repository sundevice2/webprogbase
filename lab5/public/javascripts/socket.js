const protocol = location.protocol === 'https:' ? 'wss:' : 'ws:';
const wsLocation = `${protocol}//${location.host}`;
let connection = new WebSocket(wsLocation);

async function showToast(message) {
    let toastTemplate = await (await fetch("/templates/toast.mst")).text();
    const appEl = document.getElementById("toasts");
    toastTemplate = Mustache.render(toastTemplate, JSON.parse(message));
    appEl.insertAdjacentHTML('afterbegin', toastTemplate);
    const toastOption = {
        animation: true,
        autohide: true,
        delay: 300000,
    };
    const toast = new bootstrap.Toast(document.getElementById(`toast${JSON.parse(message).course.id}`), toastOption);
    toast.show();
}

connection.addEventListener('open',
    () => console.log(`Connected to ws server: `, wsLocation));

connection.addEventListener('error',
    (err) => console.error(`ws error: ${err}`));

connection.addEventListener('message',
    (message) => {
        console.log(`ws message: ${message.data}`);
        showToast(message.data)
    });
connection.addEventListener('close',
    (mess) => {
        console.log(`ws closed: ${JSON.stringify(mess)}`);
    });