let templateStr = "";
window.addEventListener('load', async () => {
    try {
        templateStr = await (await fetch("/templates/deleteCourse.mst")).text();
        document.getElementById("deleteCourse").addEventListener("submit", deleteCourse);
    }
    catch (err) {
        console.log(err);
    }
})
async function deleteCourse(e) {
    try {
        e.preventDefault();
        document.getElementById("spinner").style.display = "";
        document.getElementById("app").style.display = "none";
        const deletedСourse = await (await fetch("/api" + e.target.attributes.action.nodeValue, { method: "POST" })).json();
        const renderedHtmlStr = Mustache.render(templateStr, deletedСourse);
        document.getElementsByClassName("modal-backdrop fade show")[0].className = "";
        document.getElementById("deleteCourse").removeEventListener("submit", deleteCourse);
        const appEl = document.getElementById('app');
        appEl.innerHTML = renderedHtmlStr;
        document.getElementById("spinner").style.display = "none";
        document.getElementById("app").style.display = "";
    }
    catch (err) {
        console.log(err)
    }

}